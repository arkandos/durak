// Node modules
var fs = require('fs'),
    vm = require('vm'),
    merge = require('deeply'),
    chalk = require('chalk'),
    es = require('event-stream');

// Gulp and plugins
var gulp = require('gulp'),
    rjs = require('gulp-requirejs'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    replace = require('gulp-replace'),
    uglify = require('gulp-uglify'),
    css = require('gulp-clean-css'),
    htmlreplace = require('gulp-html-replace'),
    eslint = require('gulp-eslint');

// Config
var requireJsRuntimeConfig = vm.runInNewContext(fs.readFileSync('src/app/require.config.js') + '; require;');
    requireJsOptimizerConfig = merge(requireJsRuntimeConfig, {
        out: 'scripts.js',
        baseUrl: './src',
        name: 'app/startup',
        paths: {
            requireLib: 'lib/requirejs/require'
        },
        include: [
            'requireLib',
            
            'components/login-page/login-page',
            'components/client-page/client-page',
            'components/game-page/game-page',
            'components/custom-game-page/custom-game-page',
            
            'components/notifications/notifications',
            'components/friend-list/friend-list',
            'components/messages/messages'
            
        ],
        insertRequire: ['app/startup'],
        bundles: {
            // If you want parts of the site to load on demand, remove them from the 'include' list
            // above, and group them into bundles here.
            // 'bundle-name': [ 'some/module', 'another/module' ],
            // 'another-bundle-name': [ 'yet-another-module' ]
            
            // 'client-page': [ 'components/client-page/client-page' ],
            // 'game-page': [ 'components/game-page/game-page' ],
        }
    });

var eslintConfig = JSON.parse(fs.readFileSync('.eslintrc', 'utf8'));

// Discovers all AMD dependencies, concatenates together all required .js files, minifies them
gulp.task('js', ['lint'], function () {
    return rjs(requireJsOptimizerConfig)
        .pipe(uglify({ preserveComments: 'some' }))
        .pipe(gulp.dest('./dist/'));
});

// Concatenates CSS files, rewrites relative paths to Bootstrap fonts, copies Bootstrap fonts
gulp.task('css', function () {
    var appCss = gulp.src('src/res/css/*.css'),
        normalizeCss = gulp.src('src/lib/normalize-css/normalize.css'),
        fontAwesomeCss = gulp.src('src/lib/font-awesome/css/font-awesome.min.css').pipe(replace(/\.\.\/fonts\//g, "/res/fonts/"));
    
    return es.concat(normalizeCss, fontAwesomeCss, appCss).pipe(concat('styles.css')).pipe(css()).pipe(gulp.dest('./dist/'));            
});

// Copies index.html, replacing <script> and <link> tags to reference production URLs
gulp.task('html', function() {
    var indexHtml = gulp.src('./src/index.html').pipe(htmlreplace({ 'css': 'styles.css', 'js': 'scripts.js' }));
    var manifestFile = gulp.src('./src/manifest.json');
    
    return es.concat(indexHtml, manifestFile).pipe(gulp.dest('./dist/'));
});

gulp.task('res', function() {
    gulp.src(['./src/res/**/*', '!src/res/css/**/*']).pipe(gulp.dest('./dist/res/'));

    var fontAwesomeRes = gulp.src('src/lib/font-awesome/fonts/*', {base: 'src/lib/font-awesome/' }),
        appRes = gulp.src(['./src/res/**/*', '!./src/res/css/**/*']);

    return es.concat(fontAwesomeRes, appRes).pipe(gulp.dest('./dist/res/'));
});

// validate all js files in src, but not external files from bower in src/lib
gulp.task('lint', function() {
    return gulp.src(['src/**/*.js', 'server/**/*.js', '!src/lib/**/*'])
        .pipe(eslint(eslintConfig))
        .pipe(eslint.format())
        .pipe(eslint.failOnError());
});

// Removes all files from ./dist/
gulp.task('clean', function() {
    return gulp.src('./dist/**/*', { read: false })
        .pipe(clean());
});

gulp.task('default', ['html', 'js', 'css', 'res'], function(callback) {
    callback();
    console.log('\nPlaced optimized files in ' + chalk.magenta('dist/\n'));
});
