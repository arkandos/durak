module.exports = {
    client: 'mysql',
    connection: require("./server/config.json").db,
    migrations: {
        directory: "./server/migrations/"
    }
};
