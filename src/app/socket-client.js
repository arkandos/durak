/* eslint-env browser */
/* global define, io */
define(["bliss", "signals"], function($, signals) {
    
    function decamelize(str, sep) {
        if (typeof str !== "string") {
            throw new TypeError("Expected a string");
        }

        var separator = typeof sep === "undefined" ? "_" : sep;

        return str
            .replace(/([a-z\d])([A-Z])/g, "$1" + separator + "$2")
            .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, "$1" + separator + "$2")
            .toLowerCase();
    }
    
    
    function SocketClient() {        
        this.socket = null;
        
        this.isConnected = false;
        this.user = null;
        
        this.connected = new signals.Signal();
        this.disconnected = new signals.Signal();
        this.goto = new signals.Signal();
        
        // at least we only have to copy the structure.
        this.socketRoutes = {
            game: {
                update: new signals.Signal(),
                playCard: true,
                pass: true
            },
            notification: {
                recieve: new signals.Signal()
            },
            friendship: {
                send: true,
                accept: true,
                deny: true,
                update: new signals.Signal()
            },
            customGame: {
                init: true,
                invite: true,
                create: true,
                accepted: new signals.Signal(),
                denied: new signals.Signal()
            },
            message: {
                send: true,
                recieve: new signals.Signal()
            }
        };

        this.bindRoutes();
    }
    
    SocketClient.prototype.login = function(username, password) {
        var self = this;
        
        if (self.isConnected) return Promise.resolve(null);
        
        return $.fetch("/user/login", {
            method: "POST",
            responseType: "json",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify({ username: username, password: password })
        }).then(function(xhr) {
            self.user = xhr.response;
            self.connect();            
        });
    };
    
    SocketClient.prototype.create = function(username, password, email) {
        var self = this;
        
        if (self.isConnected) return Promise.resolve(null);
        
        return $.fetch("/user/create", {
            method: "POST",
            responseType: "json",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify({ username: username, email: email, password: password })
        }).then(function(xhr) {
            
            self.user = xhr.response;
            self.connect();
            
        });
    };
    
    SocketClient.prototype.bindRoute = function(namespaceKey, actionKey) {
        var self = this;
        
        var namespaceName = decamelize(namespaceKey, "-");
        var actionName = decamelize(actionKey, "-");
        
        if (self.socketRoutes[namespaceKey][actionKey] instanceof signals.Signal) {
            // recieve action, copy Signal to the right field, and add socket callback
            self[namespaceKey][actionKey] = self.socketRoutes[namespaceKey][actionKey];
            
            if (self.socket !== null) {
                self.socket.on(namespaceName + "/" + actionName, function() {
                    self[namespaceKey][actionKey].dispatch.apply(
                        self[namespaceKey][actionKey],
                        Array.prototype.concat.apply([self], arguments));
                });
            }
        } else if (self.socketRoutes[namespaceKey][actionKey] === true) {
            // send action, create a function that emits the right route.
            self[namespaceKey][actionKey] = function() {
                var emitArgs = [namespaceName + "/" + actionName];
                
                if (arguments.length > 0) {
                    Array.prototype.push.apply(emitArgs, arguments);
                }
                
                self.socket.emit.apply(self.socket, emitArgs);
            };
        }
    };
    
    SocketClient.prototype.bindRoutes = function() {
        var self = this;
        
        // loop through all socketRoutes and bindRoute on them.
        for (var namespaceKey in self.socketRoutes) {
            if (self.socketRoutes.hasOwnProperty(namespaceKey)) {
                self[namespaceKey] = {};
                for (var actionKey in self.socketRoutes[namespaceKey]) {
                    if (self.socketRoutes[namespaceKey].hasOwnProperty(actionKey)) {
                        self.bindRoute(namespaceKey, actionKey);
                    }
                }
            }
        }
    };
    
    SocketClient.prototype.connect = function() {
        var self = this;
        
        // short-circuit: do not need to connect if we already are connected.
        if (self.isConnected) return;
        
        self.socket = io.connect("//" + window.location.host);
        
        self.socket.on("connect", function(client) {
            self.isConnected = true;
            self.connected.dispatch(self, client);
        });
        
        self.socket.on("disconnect", function(client) {
            self.isConnected = false;
            self.disconnected.dispatch(self, client);
        });
        
        self.socket.on("goto", function(route) {
            self.goto.dispatch(self, route);
        });
        
        self.bindRoutes();
    };
    
    return SocketClient;
});
