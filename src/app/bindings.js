/* eslint-env browser */
/* global define */

define(["knockout", "bliss"], function(ko, $) {
    
    ko.bindingHandlers.class = ko.bindingHandlers.css;
    
    ko.bindingHandlers.inlineClick = {
        preprocess : function(val) {
            return "function($data, $event) { " + val + "; }";
        },
        
        init: ko.bindingHandlers.click.init,
    };
    
    ko.bindingHandlers.slideVisible = {
        update: function(element, valueAccessor) {
            var value = ko.unwrap(valueAccessor());
            
            if (value) {
                // copy element and make it visible to figure out it's target height...
                var cloned = element._.clone();
                
                cloned.style.removeProperty("max-height");
                cloned._.style({
                    position: "absolute",
                    height: "auto",
                    width: "1000px",
                    left: "-10000em"
                });
                cloned._.after(element);

                var height = cloned.clientHeight;
                
                cloned._.remove();
                
                element._.transition({ "max-height": height + "px" });
            } else {
                element.style["overflow-y"] = "hidden";
                element._.transition({ "max-height": 0 });
            }
        }
    };
    
    ko.bindingHandlers.fadeIn = {
        update: function(element, valueAccessor) {
            var value = ko.unwrap(valueAccessor());
            
            if (value) {
                element._.style({display: "", opacity: 0});
                setTimeout(function() {
                    element._.transition({ opacity : 1});
                }, 50);
            } else {
                element._.style({display: "none", transitionProperty: "", opacity : 0});
            }
        }
    };
    
    ko.bindingHandlers.fade = {
        update: function(element, valueAccessor) {
            var value = ko.unwrap(valueAccessor());
            
            element.style.removeProperty("display");
            
            setTimeout(function() {
                element._.transition({ opacity: value ? 1 : 0 })
                    .then(function() {
                        if (!value) {
                            element.style.display = "none";
                        }
                    });
            }, 50);
            
        }
    };
    
    ko.bindingHandlers.verticalScroll = {
        preprocess : function(val) {
            return val || true;
        },
        init: function(element, valueAccessor) {
            element.addEventListener("wheel", function(evt) {
                if (evt.deltaY !== 0 && element.scrollHeight === element.clientHeight) {
                    element.scrollLeft += evt.deltaY;
                    evt.preventDefault();
                }
            });
        }
    };
    
});
