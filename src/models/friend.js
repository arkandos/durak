/* eslint-env browser */
/* global define */
define(["bliss", "knockout"], function($, ko) {
    function Friend(data) {
        $.extend(this, data, "own");

        this.id = data.id;
        this.username = ko.observable(data.username);
        this.loggedIn = ko.observable(data.logged_in); // eslint-disable-line camelcase
        this.email = ko.observable(data.email);
        
        this.messages = ko.observableArray();
        
        this.acceptedGameInvite = ko.observable(null);
        
        this.statusText = ko.pureComputed(function() {
            if (this.loggedIn()) {
                return "Online";
            }
            
            return "Offline";
        }, this);
    }
    
    Friend.prototype.update = function(data) {
        this.username(data.username);
        this.loggedIn(data.logged_in);  // eslint-disable-line camelcase
        this.email(data.email);
    };
    
    Friend.prototype.toString = function() {
        return this.username();
    };
    
    return Friend;
});
