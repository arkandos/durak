/* eslint-env browser */
/* global define */
define(["bliss"], function($) {
    
    function Notification(gameClient, data) {
        this.title = data.title;
        this.message = data.message;
        
        this.hasAccept = data.accept && data.accept.length > 0;
        
        this.gameClient = gameClient;
        this.data = data;
    }
    
    Notification.prototype.accept = function() {
        this.gameClient.acceptNotification(this);
    };
    
    Notification.prototype.deny = function() {
        this.gameClient.denyNotification(this);
    };
    
    return Notification;
});
