/* eslint-env browser */
/* global define */
define(["bliss", "knockout", "hasher"], function($, ko, hasher) {
    
    function obfuscateGameData(data) {
        data.deck = data.deck.map(function() {
            return "OO";
        });
        
        if (typeof data.players !== "undefined") {
            for (var i = 0; i < data.players.length; ++i) {
                if (data.players[i].player_id !== data._pivot_player_id) {
                    data.players[i].hand = data.players[i].hand.map(function() {
                        return "OO";
                    });
                }
            }
        }
        
        return data;
    }
    
    function Game(data) {        
        // copy data.
        var obfuscated = obfuscateGameData(data);
        
        this.id = obfuscated.id;
        this.playerId = obfuscated._pivot_player_id;
        this.playerCount = obfuscated.player_count;
        this.name = obfuscated.name;
        this.createdAt = obfuscated.created_at;
        this.trumpCard = obfuscated.trump_card;
        
        this.closedCards = ko.observableArray(obfuscated.closed_cards);
        this.currentPlayerId = ko.observable(obfuscated.current_player_id);
        this.deck = ko.observableArray(obfuscated.deck);
        this.openCards = ko.observable(obfuscated.open_cards);
        this.updatedAt = ko.observable(new Date(obfuscated.updated_at));
        this.players = ko.observableArray(obfuscated.players);
        
        this.isAttacking = ko.pureComputed(function() {            
            return (
                this.playerId === (this.currentPlayerId() === 0 ? this.playerCount - 1 : this.currentPlayerId() - 1) ||
                this.playerId === (this.currentPlayerId() === this.playerCount - 1 ? 0 : this.currentPlayerId() + 1)
            );
        }, this);
        this.isDefending = ko.pureComputed(function() {
            return this.playerId === this.currentPlayerId();
        }, this);
        
        this.infoText = ko.pureComputed(function() {
            return this.isAttacking() || this.isDefending() ? "Du bist dran!" : "";
        }, this);
        
        this.updatedAtText = ko.pureComputed(function() {
            return this.updatedAt().toDateString();
        }, this);
        
        this.open = this.open.bind(this);
    }
    
    Game.prototype.update = function(data) {
        var obfuscated = obfuscateGameData(data);
        
        this.closedCards(obfuscated.closed_cards);
        this.currentPlayerId(obfuscated.current_player_id);
        this.deck(obfuscated.deck);
        this.openCards(obfuscated.open_cards);
        this.updatedAt(new Date(obfuscated.updated_at));
        this.players(obfuscated.players);
    };
    
    Game.prototype.open = function() {
        hasher.setHash("game/" + this.id);
    };
    
    return Game;
});
