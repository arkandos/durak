/* eslint-env browser */
/* global define */
define(["knockout", "app/translate", "text!components/notifications/notifications.html"], function(ko, translate, template) {

    function Notifications(params) {
        var self = this;
        
        self.appViewModel = params.app;
        self.visible = params.visible;
        self.hasNew = params.highlighted;
                
        self.notifications = self.appViewModel.gameClient.notifications;
        
        ko.computed(function() {
            var notifications = self.notifications();
            
            if (!self.visible.peek() && notifications.length > 0) {
                self.hasNew(true);
                
                var notification = notifications[notifications.length - 1];
                
                // eslint-disable-next-line no-unused-vars
                var n = new window.Notification(notification.title, {
                    body: translate(notification.message),
                    icon: "/res/icon_192x192.png"
                });
            }
        });
    }
    
    return { viewModel: Notifications, template: template };
});
