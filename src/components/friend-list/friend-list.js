/* eslint-env browser */
/* global define */
define(["knockout", "text!components/friend-list/friend-list.html"], function(ko, template) {
    
    function FriendList(params) {
        var self = this;
        
        self.appViewModel = params.app;
        self.visible = params.visible;
                
        self.friends = self.appViewModel.gameClient.sortedFriends;
        
        self.friendRequestName = ko.observable("");
        
        self.requestFriend = self.requestFriend.bind(self);
    }
    
    FriendList.prototype.requestFriend = function() {
        this.appViewModel.gameClient.friendship.send(this.friendRequestName());
        this.friendRequestName("");
    };
    
    return { viewModel: FriendList, template: template };
});
