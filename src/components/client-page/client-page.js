/* eslint-env browser */
/* global define */
define(["knockout", "models/game", "text!components/client-page/client-page.html"], function(ko, Game, template) {
   
    function ClientPage(appViewModel) {
        var self = this;
        
        self.appViewModel = appViewModel;
        
        self.username = self.appViewModel.gameClient.username;
        self.email = self.appViewModel.gameClient.email;
        
        self.games = self.appViewModel.gameClient.sortedGames;
         
        self.createGame = self.createGame.bind(self);
        
    }
    
    ClientPage.prototype.createGame = function() {
        this.appViewModel.setHash("create");
    };
    
    return { viewModel: ClientPage, template: template };
});
