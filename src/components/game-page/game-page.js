/* eslint-env browser */
/* global define */
define(["knockout", "text!components/game-page/game-page.html"], function(ko, template) {
    
    
    function GamePage(appViewModel) {
        var self = this;
        
        try {
            if (screen.lockOrientation) {
                screen.lockOrientation("landscape");
            } else if (screen.orientation && screen.orientation.lock) {
                screen.orientation.lock("landscape").catch(function() {
                    // ignore DomException if not supported
                });
            }
        } catch (e) {
            // ignore DomException if not supported
        }
        
        self.appViewModel = appViewModel;
        self.id = parseInt(self.appViewModel.route().id, 10);
        
        // self.appViewModel.gameClient.game.open(self.id);
        
        self.game = ko.pureComputed(function() {
            var games = self.appViewModel.gameClient.games();
            
            for (var i = 0; i < games.length; ++i) {
                if (games[i].id === self.id) {
                    return games[i];
                }
            }
            
            return null;
        });
                
        self.allowDefendDrop = self.allowDefendDrop.bind(self);
        self.allowAttackDrop = self.allowAttackDrop.bind(self);
        self.attack = self.attack.bind(self);
        self.defend = self.defend.bind(self);
        self.pass = self.pass.bind(self);
        
        self.enemies = ko.pureComputed(function() {
            var game = self.game();            
            var lhsEnemies = [], rhsEnemies = [];
            
            var lhs = true;
            
            if (game.players()) {
                var players = game.players();
                
                for (var playerIndex = 0; playerIndex < players.length; ++playerIndex) {
                    var player = players[playerIndex];

                    if (player.user_id === self.appViewModel.gameClient.userId()) {
                        lhs = false;
                    } else if (player.hand.length > 0) {
                        var enemy = {
                            defender: (player.player_id === game.currentPlayerId()),
                            name: player.name,
                            hand: player.hand
                        };

                        if (lhs) {
                            lhsEnemies.push(enemy);
                        } else {
                            rhsEnemies.push(enemy);
                        }
                    }
                }
            }
            
            return rhsEnemies.concat(lhsEnemies);
        });
        
        self.openCards = ko.pureComputed(function() {
            var game = self.game(),
                openCards = game.openCards();
            
            var result = [];
            
            for (var attackCard in openCards) {
                if (openCards.hasOwnProperty(attackCard)) {
                    result.push({ attack: attackCard, defend: openCards[attackCard] });
                }
            }
            
            return result;
        });
        
        self.playerCards = ko.pureComputed(function() {
            var game = self.game();
            
            if (game.players().length > 0) {
                var cards = game.players()[game.playerId].hand;

                cards.sort(function(card1, card2) {
                    var card1IsTrump = (game.trumpCard.charAt(0) === card1.charAt(0));
                    var card2IsTrump = (game.trumpCard.charAt(0) === card2.charAt(0));

                    if (card1IsTrump && !card2IsTrump) {
                        return -1;
                    } else if (!card1IsTrump && card2IsTrump) {
                        return 1;
                    }

                    var valueDiff = ("6789XJQKA".indexOf(card1.charAt(1)) - "6789XJQKA".indexOf(card2.charAt(1)));         

                    if (valueDiff === 0) {
                        return "CSHD".indexOf(card1.charAt(0)) - "CSHD".indexOf(card2.charAt(0));
                    }

                    return valueDiff;
                });
                
                return cards;
            }
            
            return [];
        });
        
        self.gameStatusText = ko.pureComputed(function() {
            if (self.game().isAttacking()) {
                return "Du bist Angreifer!";
            } else if (self.game().isDefending()) {
                return "Du bist Verteidiger!";
            }
            
            return "Du bist nicht dran.";
        });
    }
    
    /* GamePage.prototype.onGameUpdated = function(gameClient, data) {
        if (data.success) {
            var wasAttacking = this.isAttacking(), wasDefending = this.isDefending();
            
            this.gameState(data.data);
        
            if (window.document.hidden) {
                if ((this.isAttacking() && !wasAttacking) || (this.isDefending() && !wasDefending)) {
                    window.Notification.requestPermission().then(function() {
                        // TODO translate
                        var n = new window.Notification("Du bist am Zug!");
                    });
                }
            }
        }
    }; */
    
    GamePage.prototype.validateDefendMove = function(card, toTrumpWhat, trumpCard) {
        var cardValues = "6789XJQKA";
        
        if (toTrumpWhat.charAt(0) === trumpCard.charAt(0)) {
            return (cardValues.indexOf(card.charAt(1)) > cardValues.indexOf(toTrumpWhat.charAt(1)));
        }
        
        return (card.charAt(0) === trumpCard.charAt(0) ||
                (card.charAt(0) === toTrumpWhat.charAt(0) &&
                 cardValues.indexOf(card.charAt(1)) > cardValues.indexOf(toTrumpWhat.charAt(1))));
    };
    
    GamePage.prototype.allowDefendDrop = function(event, data, model) {
        var game = this.game();
        
        return (!model.defend &&
                game.currentPlayerId() === game.playerId && 
                this.validateDefendMove(data, model.attack, game.trumpCard));
    };
    
    GamePage.prototype.validateAttackMove = function(card) {
        var game = this.game(),
            openCards = game.openCards();
                
        // check if player is in fact an attacker                
        if (game.isAttacking()) {
            
            // check if this would be the first card played, or there is already a card of this value
            // TODO: only the right attacker can play the first card.
            
            if (Object.keys(openCards).length === 0) {
                return true;
            }
            
            for (var openAttackCard in openCards) {
                if (openCards.hasOwnProperty(openAttackCard)) {
                    var openDefendCard = openCards[openAttackCard];
                    
                    if (openAttackCard.charAt(1) === card.charAt(1) ||
                        (!!openDefendCard && openDefendCard.charAt(1) === card.charAt(1))) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    };
    
    GamePage.prototype.allowAttackDrop = function(event, card, model) {
        return this.validateAttackMove(card);
    };
    
    GamePage.prototype.defend = function(card, model) {
        if (this.validateDefendMove(card, model.attack, this.game().trumpCard)) {
            console.log("PLAY CARD: " + card + " on top of " + model.attack);
            this.appViewModel.gameClient.game.playCard(this.id, card, model.attack);
        }
    };
    
    GamePage.prototype.attack = function(card, model) {
        if (this.validateAttackMove(card)) {
            console.log("PLAY CARD: " + card);
            this.appViewModel.gameClient.game.playCard(this.id, card);
        }
    };
    
    GamePage.prototype.pass = function() {
        this.appViewModel.gameClient.game.pass(this.id);
    };
    
    GamePage.prototype.dispose = function() {
        try {
            if (screen.unlockOrientation) {
                screen.unlockOrientation();
            } else if (screen.orientation && screen.orientation.unlock) {
                screen.orientation.unlock().catch(function() {
                    // ignore DomException if not supported
                });
            }
        } catch (e) {
            // ignore DomException if not supported
        }
    };
    
    return { viewModel: GamePage, template: template };
});
