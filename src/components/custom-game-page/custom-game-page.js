/* eslint-env browser */
/* global define */
define(["knockout", "text!components/custom-game-page/custom-game-page.html"], function(ko, template) {
   
    function CustomGamePage(appViewModel) {
        var self = this;
        
        self.appViewModel = appViewModel;
        
        self.gameName = ko.observable(appViewModel.gameClient.username() + "'s Spiel");
        self.invitedFriends = ko.observableArray([]);
        
        self.friends = self.appViewModel.gameClient.onlineFriends;
        self.inviteFriend = ko.observable(null);
        
        self.inviteFriendAlreadyInvited = ko.pureComputed(function() {
            return self.invitedFriends.indexOf(self.inviteFriend()) >= 0;
        });
        
        self.onInviteAccepted = self.onInviteAccepted.bind(self);
        self.onInviteDenied = self.onInviteDenied.bind(self);
        
        self.appViewModel.gameClient.socketClient.customGame.accepted.add(self.onInviteAccepted);
        self.appViewModel.gameClient.socketClient.customGame.denied.add(self.onInviteDenied);
        
        
        self.appViewModel.gameClient.customGame.init();
    }
    
    CustomGamePage.prototype.dispose = function() {
        this.appViewModel.gameClient.socketClient.customGame.accepted.remove(this.onInviteAccepted);
        this.appViewModel.gameClient.socketClient.customGame.denied.remove(this.onInviteDenied);
    };
    
    CustomGamePage.prototype.onInviteAccepted = function(client, friendId) {
        var invitedFriends = this.invitedFriends();
        
        for (var i = 0; i < invitedFriends.length; ++i) {
            if (invitedFriends[i].id === friendId) {
                invitedFriends[i].acceptedGameInvite(true);
                break;
            }
        }
    };
    
    CustomGamePage.prototype.onInviteDenied = function(client, friendId) {
        var invitedFriends = this.invitedFriends();
        
        for (var i = 0; i < invitedFriends.length; ++i) {
            if (invitedFriends[i].id === friendId) {
                invitedFriends[i].acceptedGameInvite(false);
                break;
            }
        }
    };
    
    CustomGamePage.prototype.invite = function() {
        if (!this.inviteFriendAlreadyInvited()) {
            var friend = this.inviteFriend();
            
            friend.acceptedGameInvite(null);
            
            this.appViewModel.gameClient.customGame.invite(this.gameName(), ko.unwrap(friend.username));
            this.invitedFriends.push(friend);
        }
    };
    
    CustomGamePage.prototype.createGame = function() {
        var invitedFriends = this.invitedFriends(),
            playerIds = [this.appViewModel.gameClient.userId()];
        
        for (var i = 0; i < invitedFriends.length; ++i) {
            if (invitedFriends[i].acceptedGameInvite()) {
                playerIds.push(invitedFriends[i].id);
            }
        }
        
        this.appViewModel.gameClient.customGame.create(this.gameName(), playerIds);
    };
    
    return { viewModel: CustomGamePage, template: template };
});
