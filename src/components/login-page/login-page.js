/* eslint-env browser */
/* global define */
define(["knockout", "text!components/login-page/login-page.html"], function(ko, template) {
    
    function LoginPage(appViewModel) {
        var self = this;
        
        self.appViewModel = appViewModel;
        
        if (self.appViewModel.gameClient.socketClient.isConnected) {
            self.appViewModel.redirectLogin();
        }
                
        self.username = ko.observable("");
        self.password = ko.observable("");
        self.email = ko.observable("");
        
        self.createNew = ko.observable(false);
        
        self.result = ko.observable("");
        
        self.loginOrCreate = self.loginOrCreate.bind(self);
        self.toggleNew = self.toggleNew.bind(self);
    }
    
    LoginPage.prototype.loginOrCreate = function() {
        var self = this;
        
        self.result("");
        
        if (self.createNew()) {
            self.appViewModel.gameClient.create(self.username(), self.password(), self.email())
                .catch(function(error) {
                    if (error) {
                        self.result("Es gibt bereits einen Spieler mit diesem Benutzernamen oder dieser E-Mail!");
                    }
                });
        } else {
            self.appViewModel.gameClient.login(self.username(), self.password())
                .catch(function(error) {
                    if (error) {
                        self.result("Benutzername oder Passwort falsch!");
                    }
                });
        }
    };
    
    LoginPage.prototype.toggleNew = function() {
        this.createNew(!this.createNew());
    };
    
    return { viewModel: LoginPage, template: template };
});
