Durak
=====

This is a traditional russian card game, implemented as a online multiplayer game, completely using Javascript.

Installation
------------

Requires an up-to-date npm and node installation, as well as a MySQL (>= 5.4) database. Installation was tested on a Ubuntu 14.04 machine.

**Note**: On a typical Ubuntu installation, the Node.JS binary is called _nodejs_ instead of just _node_!

### 1. Install dependencies

    $ npm install --production
    $ node node_modules/bower/bin/bower install
    
### 2. Configure database connection

You should find a _config.json.dist_ file in the _server_ directory. Rename or copy this file to _server/config.json_, and input your database connection there.

    {
        "port": 8080, // port the http server will listen to
        "production": false, // serve files under ./dist/ vs. ./src/, this is different from node production!
        "db": {
            "host": "127.0.0.1", // database server address
            "user": "durak", // database user
            "password": "password", // database user password (plaintext)
            "database": "durak", // database to use
            "charset": "utf8"
        }
    }

### 3. Install database schema
    
    $ node node_modules/knex/lib/bin/cli.js migrate:latest
    
### 4. Run the server!

    $ cd server && node index.js