var passportSocketIo = require("passport.socketio"),
    io = require("../init/socketio"),
    Notification = require("../models/notification");

module.exports = {
    send: function(userId, title, message, data, accept, deny) {
        var notification = Notification.forge({
            user_id: userId, //eslint-disable-line camelcase
            title: title,
            message: message || null,
            data: data || null,
            accept: accept,
            deny: deny
        });
        
        console.log("NOTIFICATION: " + JSON.stringify(notification));
        
        var notificationSent = false;
        
        passportSocketIo.filterSocketsByUser(io, function(user) {
            return (user.id === userId);
        }).forEach(function(userSocket) {
            userSocket.emit("notification/recieve", notification.toJSON());
            notificationSent = true;
            
            return false;
        });
        
        if (!notificationSent) {
            notification.save();
        }
    }
};
