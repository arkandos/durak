var Notification = require("../helpers/notification");

var GameHelper = {
    
    CARD_COLORS: "CSHD",
    CARD_VALUES: "6789XJQKA",
    
    createShuffledDeck: function() {
        var deck = "C6C7C8C9CXCJCQCKCAS6S7S8S9SXSJSQSKSAH6H7H8H9HXHJHQHKHAD6D7D8D9DXDJDQDKDA".match(/../g);
        var deckShuffleCounter = deck.length;

        while (deckShuffleCounter > 0) {
            var deckSuffleIndex = Math.floor(Math.random() * deckShuffleCounter);

            deckShuffleCounter -= 1;

            var deckShuffleTemp = deck[deckShuffleCounter];

            deck[deckShuffleCounter] = deck[deckSuffleIndex];
            deck[deckSuffleIndex] = deckShuffleTemp;
        }
        
        return deck;
    },
    
    isValidCard: function(card) {
        return /^[CSHD][6789XJQKA]$/.test(card);
    },
    
    getCurrentPlayers: function(game) {
        var players  = game.related("players").models,
            currentPlayerId = game.get("current_player_id");
        
        var defender = players[currentPlayerId];
        var attacker1 = currentPlayerId === 0 ? players[players.length - 1] : players[currentPlayerId - 1];
        var attacker2 = currentPlayerId === players.length - 1 ? players[0] : players[currentPlayerId + 1];
        
        return {
            defender: defender,
            attacker1: attacker1,
            attacker2: attacker2
        };
    },
    
    playCard: function(game, userId, card, toTrumpWhat) {
        var players = game.related("players").models;            
        // check if user is part of this game
        var player = null;

        for (var i = 0; i < players.length; ++i) {
            if (players[i].get("user_id") === userId) {
                player = players[i];
                break;
            }
        }

        if (player === null) {
            Notification.send(userId, "Fehler", "Du bist nicht Teil dieses Spiels!");
            
            return false;
        }

        if (!GameHelper.isValidCard(card)) {
            Notification.send(userId, "Fehler", "Spiele eine gültige Karte!");
            
            return false;
        }

        if (toTrumpWhat && !GameHelper.isValidCard(toTrumpWhat)) {
            Notification.send(userId, "Fehler", "Steche eine gültige Karte!");
            
            return false;
        }

        if (player.get("hand").indexOf(card) < 0) {
            Notification.send(userId, "Fehler", "Versuche Karte zu spielen, die du nicht auf der Hand hast!");
            
            return false;
        }

        var currentPlayers = GameHelper.getCurrentPlayers(game);
        var openCards = game.get("open_cards");

        if (currentPlayers.attacker1.id === player.id || currentPlayers.attacker2.id === player.id) {

            if (toTrumpWhat) {
                Notification.send(userId, "Fehler", "Du kannst als Angreifer keine Karte stechen!");
                
                return false;
            }

            var openCardsCount = 0;

            for (var openCardsAttackCard in openCards) {
                if (openCards.hasOwnProperty(openCardsAttackCard) && !openCards[openCardsAttackCard]) {
                    openCardsCount += 1;
                }
            }

            if (openCardsCount >= currentPlayers.defender.get("hand").length) {
                Notification.send(userId, "Fehler", "Es liegen bereits so viele Karten aus, wie der Verteidiger Handkarten hat!");
                
                return false;
            }

            // noch keine Karte gespielt
            if (Object.keys(openCards).length === 0) {
                // spieler muss erster angreifer sein
                if (player.id !== currentPlayers.attacker1.id) {
                    Notification.send(userId, "Fehler", "Warte, bis der rechte Angreifer eine Karte gespielt hat!");
                    
                    return false;
                }

            } else {

                // karte muss vom selben wert sein, wie eine bereits gespielte karte
                var possibleCardValues = "";

                for (var openAttackCard in openCards) {
                    if (openCards.hasOwnProperty(openAttackCard)) {
                        possibleCardValues += openAttackCard.charAt(1);

                        if (openCards[openAttackCard]) {
                            possibleCardValues += openCards[openAttackCard].charAt(1);
                        }
                    }
                }

                if (possibleCardValues.indexOf(card.charAt(1)) < 0) {
                    Notification.send(userId, "Fehler", "Karte muss vom selben Wert sein wie eine bereits gespielte Karte!");
                    
                    return false;
                }
            }

        } else if (currentPlayers.defender.id === player.id) {

            if (!toTrumpWhat) {
                Notification.send(userId, "Fehler", "Du musst eine Karte auf eine andere legen!");
                
                return false;
            }

            var trumpCard = game.get("trump_card");

            if (!openCards.hasOwnProperty(toTrumpWhat)) {
                Notification.send(userId, "Fehler", "Du kannst nur Karten stechen, die auch gespielt wurden!");
                
                return false;
            }

            if (openCards[toTrumpWhat] !== false) {
                Notification.send(userId, "Fehler", "Du kannst jede Karte nur einmal stechen!");
                
                return false;
            }

            if (toTrumpWhat.charAt(0) === trumpCard.charAt(0)) {
                if (card.charAt(0) !== trumpCard.charAt(0)) {
                    Notification.send(userId, "Fehler", "Du kannst einen Trumpf nur mit einem anderen Trumpf stechen!");
                    
                    return false;
                }
                if (GameHelper.CARD_VALUES.indexOf(card.charAt(1)) <= GameHelper.CARD_VALUES.indexOf(toTrumpWhat.charAt(1))) {
                    Notification.send(userId, "Fehler", "Du kannst einen Trumpf nur mit einem höheren Trumpf stechen!");
                    
                    return false;
                }
            } else if (card.charAt(0) !== trumpCard.charAt(0)) {

                if (card.charAt(0) !== toTrumpWhat.charAt(0)) {
                    Notification.send(userId, "Fehler", "Du kannst eine Farbe nur mit einer Karte der selben Farbe oder einem Trumpf stechen!");
                    
                    return false;
                }

                if (GameHelper.CARD_VALUES.indexOf(card.charAt(1)) <= GameHelper.CARD_VALUES.indexOf(toTrumpWhat.charAt(1))) {
                    Notification.send(userId, "Fehler", "Du kannst eine Farbe nur mit einer höheren Farbe oder einem Trumpf stechen!");
                    
                    return false;
                }

            }

        } else {
            Notification.send(userId, "Fehler", "Du bist nicht am Zug!");
            
            return false;
        }

        // move seems valid if we are still here.
        var hand = player.get("hand");
        
        hand.splice(hand.indexOf(card), 1);

        player.set("hand", hand);
        if (toTrumpWhat) {
            openCards[toTrumpWhat] = card;
        } else {
            openCards[card] = false;
        }

        game.set("open_cards", openCards);
        
        return true;
    },
    
    pass: function(game, userId) {
        var currentPlayers = GameHelper.getCurrentPlayers(game);
        var passedPlayer = null;
        
        if (currentPlayers.defender.get("user_id") === userId) {
            passedPlayer = currentPlayers.defender;
        } else if (currentPlayers.attacker1.get("user_id") === userId) {
            passedPlayer = currentPlayers.attacker1;
        } else if (currentPlayers.attacker2.get("user_id") === userId) {
            passedPlayer = currentPlayers.attacker2;
        }
        
        if (passedPlayer !== null && !passedPlayer.get("has_passed")) {
            passedPlayer.set("has_passed", true);
            
            return true;
        }
        
        return false;
    },
    
    checkNextRound: function(game) {
        var currentPlayers = GameHelper.getCurrentPlayers(game);
        var openCards = game.get("open_cards");

        // check if we need to do the thing

        if (Object.keys(openCards).length === 0) {
            return false;
        }

        if (currentPlayers.defender.get("has_passed")) {
            return true;
        }

        if (currentPlayers.defender.get("hand").length === 0) {
            return true;
        }

        var possibleValues = "";
        var stillNeedsToTrump = false;

        for (var attackOpenCard in openCards) {
            if (openCards.hasOwnProperty(attackOpenCard)) {
                possibleValues += attackOpenCard.charAt(1);

                if (openCards[attackOpenCard]) {
                    possibleValues += openCards[attackOpenCard].charAt(1);
                } else {
                    stillNeedsToTrump = true;
                    break;
                }
            }
        }

        if (stillNeedsToTrump) {
            return false;
        }


        function checkPlayableCards(playerObj) {
            var hasPlayableCards = (possibleValues.length === 0);
            var hand = playerObj.get("hand");
            
            for (var handIndex = 0; handIndex < hand.length; ++handIndex) {
                if (possibleValues.indexOf(hand[handIndex].charAt(1)) >= 0) {
                    hasPlayableCards = true;
                    break;
                }
            }

            return hasPlayableCards;
        }

        if ((currentPlayers.attacker1.get("has_passed") || !checkPlayableCards(currentPlayers.attacker1)) &&
            (currentPlayers.attacker2.get("has_passed") || !checkPlayableCards(currentPlayers.attacker2))) {
            return true;
        }

        return false;
    },
    
    checkAndStartNextRound: function(game) {
        // check for next game first
        if (GameHelper.checkAndStartNextGame(game)) {
            return true;
        }
        
        // check if we need to do the thing

        if (!GameHelper.checkNextRound(game)) {
            return false;
        }

        // if we still here, actually do the thing
        var currentPlayers = GameHelper.getCurrentPlayers(game);
        var openCards = game.get("open_cards");
        var playerLost = false;

        for (var checkOpenCard in openCards) {
            if (openCards.hasOwnProperty(checkOpenCard) && !openCards[checkOpenCard]) {
                playerLost = true;
                break;
            }
        }

        if (playerLost) {
            // add cards to the current player's hand
            for (var playerLostOpenCard in openCards) {
                if (openCards.hasOwnProperty(playerLostOpenCard)) {
                    currentPlayers.defender.get("hand").push(playerLostOpenCard);

                    if (openCards[playerLostOpenCard]) {
                        currentPlayers.defender.get("hand").push(openCards[playerLostOpenCard]);
                    }
                }
            }
        } else {
            // add cards to the closed list
            for (var playerWonOpenCard in openCards) {
                if (openCards.hasOwnProperty(playerWonOpenCard)) {
                    game.get("closed_cards").push(playerWonOpenCard);

                    if (openCards[playerWonOpenCard]) {
                        game.get("closed_cards").push(openCards[playerWonOpenCard]);
                    }
                }
            }
        }

        // reset round state
        game.set("open_cards", {});
        currentPlayers.defender.set("has_passed", false);
        currentPlayers.attacker1.set("has_passed", false);
        currentPlayers.attacker2.set("has_passed", false);

        // players draw cards.

        function redraw(playerObj) {
            while (playerObj.get("hand").length < game.get("initial_card_count") && game.get("deck").length > 0) {
                playerObj.get("hand").push(game.get("deck").shift());
            }
        }

        redraw(currentPlayers.attacker1);
        redraw(currentPlayers.attacker2);
        redraw(currentPlayers.defender);

        // move to next player.
        var players = game.related("players").models;

        if (playerLost) {
            game.set("current_player_id", (game.get("current_player_id") + 2) % players.length);
        } else {
            game.set("current_player_id", (game.get("current_player_id") + 1) % players.length);
        }

        // this looks pretty ugly after refactoring, but it will do for now.
        while (players[game.get("current_player_id")].get("hand").length === 0) {
            game.set("current_player_id", (game.get("current_player_id") + 1) % players.length);
        }

        return true;
    },
    
    checkNextGame: function(game) {
        if (game.get("deck").length > 0) {
            return -1;
        }

        var players = game.related("players").models;
        var playersInGameCount = 0, lastPlayerId = -1;

        for (var playerIndex = 0; playerIndex < players.length; ++playerIndex) {
            if (players[playerIndex].get("hand").length > 0) {
                playersInGameCount += 1;
                lastPlayerId = players[playerIndex].get("player_id");
            }
        }

        if (playersInGameCount === 1) {
            return lastPlayerId;
        }

        return -1;
    },
    
    checkAndStartNextGame: function(game) {
        var durak = GameHelper.checkNextGame(game);
    
        if (durak < 0) {
            return false;
        }

        // still here, set up everything for a new round!
        var deck = GameHelper.createShuffledDeck();

        var players = game.related("players").models;
        
        for (var i = 0; i < players.length; ++i) {
            players[i].set("hand", deck.splice(0, game.get("initial_card_count")));
            players[i].set("has_passed", false);
        }

        var trumpCard = deck.shift();
        
        game.set("deck", deck);
        game.set("trump_card", trumpCard);
        game.set("current_player_id", durak);
        game.set("closed_cards", []);
        game.set("open_cards", {});
        game.set("finished_rounds", game.get("finished_rounds") + 1);
        
        return true;
    }

};

module.exports = GameHelper;
