/* eslint-env node */
/* eslint newline-per-chained-call: "off" */
module.exports = {
    up: function(knex, Promise) {
        return Promise.all([
            knex.schema.createTable("users", function(table) {
                table.increments("id").primary();
                table.string("username").unique();
                table.string("email").unique();
                table.string("password");
                table.boolean("logged_in");
                table.dateTime("last_login");
                table.dateTime("created_at");
                table.dateTime("updated_at");
            }),
            knex.schema.createTable("friendships", function(table) {
                table.increments("id").primary();
                table.integer("user1_id").references("users.id").onDelete("cascade").index();
                table.integer("user2_id").references("users.id").onDelete("cascade").index();
                table.dateTime("created_at");
                table.dateTime("updated_at");

                table.index(["user1_id", "user2_id"]);
            }),
            knex.schema.createTable("notifications", function(table) {
                table.increments("id").primary();
                table.integer("user_id").references("users.id").onDelete("cascade").index();
                table.string("title");
                table.json("message");
                table.json("data");
                table.string("accept", 63);
                table.string("deny", 63);
                table.dateTime("created_at");
                table.dateTime("updated_at");
            }),
            knex.schema.createTable("games", function(table) {
                table.increments("id").primary();
                table.string("name");
                table.integer("player_count");
                table.integer("initial_card_count");
                table.string("trump_card", 2);
                table.integer("finished_rounds");
                table.string("open_cards", 72);
                table.string("closed_cards", 72);
                table.string("deck", 72);
                table.integer("current_player_id"); // not really a foreign key
                table.dateTime("created_at");
                table.dateTime("updated_at");
            }),
            knex.schema.createTable("players", function(table) {
                table.increments("id").primary();
                table.integer("user_id").references("users.id").index();
                table.integer("game_id").references("games.id").onDelete("cascade").index();
                table.integer("player_id"); // not really a foreign key (it is, but the still have a real one because normalisation...)
                table.string("name");
                table.string("hand", 72);
                table.boolean("has_passed");
            }),
        ]);
    },
    
    down: function(knex, Promise) {
        return Promise.all([
            knex.schema.dropTable("players"),
            knex.schema.dropTable("games"),
            knex.schema.dropTable("notifications"),
            knex.schema.dropTable("friendships"),
            knex.schema.dropTable("users"),
        ]);
    }
};
