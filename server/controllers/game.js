var passportSocketIo = require("passport.socketio"),
    io = require("../init/socketio"),
    Game = require("../models/game"),
    GameHelper = require("../helpers/game"),
    Notification = require("../helpers/notification");

function sendGameUpdate(game, delay) {
    var players = game.related("players").models,
        userToPlayerId = {};
    
    for (var i = 0; i < players.length; ++i) {
        userToPlayerId[players[i].get("user_id")] = players[i].get("player_id");
    }
    
    
    passportSocketIo.filterSocketsByUser(io, function(socketUser) {
        return userToPlayerId.hasOwnProperty(socketUser.id);
    }).forEach(function(userSocket) {
        var gameData = game.toJSON();
        
        gameData._pivot_user_id = userSocket.user.id; //eslint-disable-line camelcase
        gameData._pivot_game_id = gameData.id; //eslint-disable-line camelcase
        gameData._pivot_player_id = userToPlayerId[userSocket.user.id]; //eslint-disable-line camelcase
        
        userSocket.emit("game/update", gameData);
    });
    
    return game.saveWithPlayers().then(function() {
        // let's have a delay here to not just be instant
        setTimeout(function() {
            sendNextRoundUpdate(game); // eslint-disable-line no-use-before-define
        }, delay || 3000);
    }); 
}

function sendNextRoundUpdate(game) {
    if (GameHelper.checkAndStartNextRound(game)) {
        sendGameUpdate(game); // eslint-disable-line no-use-before-define
    }
}

module.exports = {

    socketPlayCard: function(gameId, card, toTrumpWhat) {
        var self = this;
        
        console.log("PLAY CARD " + this.user.get("username") + " IN GAME " + gameId + ": " + card + " => " + toTrumpWhat);
        
        Game.where("id", gameId).fetch({required: true, withRelated: "players"})
            .then(function(game) {
                if (GameHelper.playCard(game, self.user.id, card, toTrumpWhat)) {
                    return sendGameUpdate(game);
                }

                return null;
            })
            .catch(function() {
                Notification.send(self.user.id, "Fehler", "Spiel " + gameId + " nicht gefunden!");
            });
    },
    
    socketPass: function(gameId) {
        var self = this;
        
        console.log("PASS " + self.user.get("username") + " IN GAME " + gameId);
        
        Game.where("id", gameId).fetch({required: true, withRelated: "players"})
            .then(function(game) {
                if (GameHelper.pass(game, self.user.id)) {
                    return sendNextRoundUpdate(game);
                }
            
                return null;
            })
            .catch(function(err) {
                console.log(err);
                Notification.send(self.user.id, "Fehler", "Spiel " + gameId + " nicht gefunden!");
            });
    }
    
};
