var passportSocketIo = require("passport.socketio"),
    io = require("../init/socketio"),
    User = require("../models/user"),
    Game = require("../models/game"),
    Notification = require("../helpers/notification");

var CUSTOM_GAME_ACCEPTED_PLAYERS = {};

module.exports = {
    socketInit: function() {
        CUSTOM_GAME_ACCEPTED_PLAYERS[this.user.id] = [];
    },
    
    socketInvite: function(name, playerName) {
        var self = this;
        
        User.where("username", playerName).fetch({ required: true })
            .then(function(player) {
                if (player.get("logged_in")) {
                    Notification.send(player.id,
                                      "Einladung",
                                      ["%s hat dich zu seinem Spiel \"%s\" eingeladen!", self.user.get("username"), name],
                                      self.user.id,
                                      "custom-game/accept",
                                      "custom-game/deny");
                } else {
                    Notification.send(self.user.id, "Fehler", ["Spieler %s ist nicht online!", player.get("username")]);
                }
            })
            .catch(function() {
                Notification.send(self.user.id, "Fehler", ["Es konnte kein Spieler mit dem Namen %s gefunden werden!", playerName]);
            });
    },
    
    socketAccept: function(gameOwnerId) {
        var self = this;
        
        if (CUSTOM_GAME_ACCEPTED_PLAYERS.hasOwnProperty(gameOwnerId)) {
            CUSTOM_GAME_ACCEPTED_PLAYERS[gameOwnerId].push(self.user.id);
            
            passportSocketIo.filterSocketsByUser(io, function(user) {
                return user.id === gameOwnerId;
            }).forEach(function(socket) {
                socket.emit("custom-game/accepted", self.user.id);
            });
        }
    },
    
    socketDeny: function(gameOwnerId) {
        var self = this;
        
        if (CUSTOM_GAME_ACCEPTED_PLAYERS.hasOwnProperty(gameOwnerId)) {            
            passportSocketIo.filterSocketsByUser(io, function(user) {
                return user.id === gameOwnerId;
            }).forEach(function(socket) {
                socket.emit("custom-game/denied", self.user.id);
            });
        }
    },
    
    socketCreate: function(name, playerIds) {
        var self = this;
        
        if (CUSTOM_GAME_ACCEPTED_PLAYERS.hasOwnProperty(self.user.id)) {
            console.log("accepted players: " + JSON.stringify(CUSTOM_GAME_ACCEPTED_PLAYERS[self.user.id]));
            var users = playerIds.filter(function(playerId) {
                return playerId === self.user.id || CUSTOM_GAME_ACCEPTED_PLAYERS[self.user.id].indexOf(playerId) >= 0;
            });
                        
            if (users.length >= 2) {
                Game.create(name, 7, users, Math.floor(Math.random() * users.length))
                    .then(function(game) {
                        delete CUSTOM_GAME_ACCEPTED_PLAYERS[self.user.id];
                    
                        // TODO: Register game to CACHE ?
                        // Also, loading the players kinda seems reduntant here now.
                        passportSocketIo.filterSocketsByUser(io, function(user) {
                            return users.indexOf(user.id) >= 0;
                        }).forEach(function(socket) {
                            var gameData = game.toJSON();
                            
                            gameData._pivot_user_id = socket.user.id; // eslint-disable-line camelcase
                            gameData._pivot_game_id = gameData.id; // eslint-disable-line camelcase
                            gameData._pivot_player_id = users.indexOf(socket.user.id); // eslint-disable-line camelcase
                            
                            socket.emit("game/update", gameData);
                            socket.emit("goto", "game/" + gameData.id);
                        });
                        
                    });
            } else {
                Notification.send(self.user.id, "Fehler", "Du brauchst Spieler, um ein Spiel zu starten!");
            }
        }
    }
};
