var Promise = require("bluebird"),
    passport = require("../init/passport"),
    User = require("../models/user");

module.exports = {
    
    postLogin: function(req, res, next) {
        passport.authenticate("local", function(err1, user, info) {     
            if (err1) {
                return res.status(500).json(err1);
            }
            if (!user) {
                return res.status(401).json(info.message);
            }

            return req.login(user, function(err2) {
                if (err2) {
                    next(err2);
                }

                return res.json(user);
            });
        })(req, res, next);
    },

    postCreate: function(req, res) {
        var username = req.body.username,
            email = req.body.email,
            password = req.body.password;
        
        User.where("username", username).fetch()
            .then(function(model) {
                if (model) {
                    res.status(401).json({ message: "Benutzer existiert bereits!"});
                    throw new Promise.CancellationError();
                }
            
                return User.where("email", email).fetch();
            })
            .then(function(model) {
                if (model) {
                    res.status(401).json({ message: "Email existiert bereits!" });
                    throw new Promise.CancellationError();
                }
            
                return User.forge({ username: username, email: email, password: password }).save();
            })
            .then(function(model) {
                if (model) {
                    req.login(model, function(err) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.json(model);
                        }
                    });
                }
            })
            .catch(Promise.CancellationError, function() {})
            .catch(function(err) {
                res.status(500).json(err);
            });
    },
    
    socketConnect: function(socket) {
        socket.user.set("logged_in", true).set("last_login", new Date()).save();
        socket.user.related("notifications").fetch()
            .then(function(collection) {
                return collection.invokeThen("destroy");
            });
    },
    
    socketDisconnect: function(socket) {
        socket.user.set("logged_in", false).save();
    },
};

