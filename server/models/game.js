/* eslint-env node */
/* eslint camelcase: "off" */

var bookshelf = require("../init/bookshelf"),
    Promise = require("bluebird"),
    User = require("./user"),
    Player = require("./player"),
    GameHelper = require("../helpers/game");

var Game = bookshelf.Model.extend({
    tableName: "games",
    hasTimestamps: true,
    
    connectedPlayers: [],
    
    players: function() {
        return this.hasMany("Player").orderBy("player_id", "ASC");
    },
    
    currentPlayer: function() {
        return this.belongsTo("Player", "current_player_id");
    },
    
    parse: function(attrs) {
        if (attrs.open_cards) {
            var openArray = [].concat(attrs.open_cards.match(/../g));
            
            attrs.open_cards = {};
            for (var i = 0; i < openArray.length; i += 2) {
                if (openArray[i + 1] === "XX") {
                    attrs.open_cards[openArray[i]] = false;
                } else {
                    attrs.open_cards[openArray[i]] = openArray[i + 1];
                }
            }
        } else if (typeof attrs.open_cards !== "undefined") {
            attrs.open_cards = {};
        }
        
        if (attrs.closed_cards) {
            attrs.closed_cards = [].concat(attrs.closed_cards.match(/../g));
        } else if (typeof attrs.closed_cards !== "undefined") {
            attrs.closed_cards = [];
        }
        
        if (attrs.deck) {
            attrs.deck = [].concat(attrs.deck.match(/../g));
        } else if (typeof attrs.deck !== "undefined") {
            attrs.deck = [];
        }
        
        return attrs;
    },
    
    format: function(attrs) {
        var openCards = attrs.open_cards;
        
        attrs.open_cards = "";
        for (var card in openCards) {
            if (openCards.hasOwnProperty(card)) {
                
                attrs.open_cards += card;
                
                if (openCards[card]) {
                    attrs.open_cards += openCards[card];
                } else {
                    attrs.open_cards += "XX";
                }
            }    
        }
        
        attrs.closed_cards = attrs.closed_cards.join("");
        attrs.deck = attrs.deck.join("");
        
        return attrs;
    },
    
    saveWithPlayers: function() {
        var self = this;
        
        return bookshelf.transaction(function(t) {
            return self.save(null, {transacting: t})
                .tap(function(game) {
                    return game.related("players").invokeThen("save");
                });
        });
    }
});

Game.create = function(name, initialCardCount, userIds, initialPlayer) {
    var deck = GameHelper.createShuffledDeck();

    var playerCards = [];
    
    for (var userIndex = 0; userIndex < userIds.length; ++userIndex) {
        playerCards.push(deck.splice(0, initialCardCount));
    }

    var trumpCard = deck.shift();

    return User.where("id", "in", userIds).fetchAll({ required: true, columns: ["id", "username"] })
        .then(function(users) {
            return bookshelf.transaction(function(t) {
                return Game.forge({
                    name: name,
                    player_count: userIds.length,
                    initial_card_count: initialCardCount,
                    trump_card: trumpCard,
                    finished_rounds: 0,
                    open_cards: {},
                    closed_cards: [],
                    deck: deck,
                    current_player_id: initialPlayer
                })
                    .save(null, {transacting: t})
                    .tap(function(game) {
                        return Promise.map(userIds.map(function(userId, playerId) {
                            return {
                                user_id: userId,
                                player_id: playerId,
                                name: users.get(userId).get("username"),
                                hand: playerCards[playerId],
                                has_passed: false
                            };
                        }), function(playerData) {
                            return Player.forge(playerData).save({"game_id": game.id}, {transacting: t});
                        });
                    });
            }).tap(function(game) {
                return game.related("players").fetch();
            });
        });
    
};

module.exports = bookshelf.model("Game", Game);

