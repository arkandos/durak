var bookshelf = require("../init/bookshelf"),
    bcrypt = require("bcrypt-nodejs"),
    Promise = require("bluebird");

require("./friendship");

var SECRET = bcrypt.genSaltSync(10);

var User = bookshelf.Model.extend({
    tableName: "users",
    hasTimestamps: true,
    
    initialize: function() {
        bookshelf.Model.prototype.initialize.apply(this, arguments);
        this.on("saving", this.hashPassword, this);
    },
    
    friends: function() {
        return this
            .belongsToMany("User")
            .through("Friendship", "user2_id", "user1_id")
            .query(function(qb) {
                return qb.whereRaw("EXISTS(SELECT 1 FROM friendships f WHERE f.user2_id = friendships.user1_id AND f.user1_id = friendships.user2_id)");
            });
    },
    
    notifications: function() {
        return this.hasMany("Notification");
    },
    
    games: function() {
        return this
            .belongsToMany("Game")
            .through("Player")
            .withPivot(["player_id"])
            .orderBy("updated_at", "DESC")
            .query(function(qb) {
                var lastWeek = new Date();
            
                lastWeek.setDate(lastWeek.getDate() - lastWeek.getDay() - 7);
            
                return qb.where("updated_at", ">", lastWeek);
            });
    },
    
    hashPassword: function(model, attrs, options) {
        return new Promise(function(resolve, reject) {
            if (model.hasChanged("password")) {
                bcrypt.hash(model.attributes.password, SECRET, null, function(err, hash) {
                    if (err) reject(err);
                    model.set("password", hash);
                    resolve(hash);
                });
            } else {
                resolve(model.get("password"));
            }
        });
    },
    
    validatePassword: function(password) {
        return bcrypt.compareSync(password, this.get("password"));
    }
});

module.exports = bookshelf.model("User", User);
