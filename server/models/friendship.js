var bookshelf = require("../init/bookshelf.js");

require("./user");

var Friendship = bookshelf.Model.extend({
    tableName: "friendships",
    hasTimestamps: true,
    
    user: function() {
        return this.belongsTo("User", "user1_id");
    },
    
    friend: function() {
        return this.belongsTo("User", "user2_id");
    }
});

module.exports = bookshelf.model("Friendship", Friendship);
