var bookshelf = require("../init/bookshelf");

var Player = bookshelf.Model.extend({
    tableName: "players",
    
    game: function() {
        return this.belongsTo("Game");
    },
    
    user: function() {
        return this.belongsTo("User");
    },
    
    format: function(attrs) {
        if (typeof attrs.hand !== "undefined") {
            attrs.hand = attrs.hand.join("");
        }
        
        return attrs;
    },
    
    parse: function(attrs) {
        if (typeof attrs !== "undefined") { // somehow this gets called all the time, even tho it shouldn't
            if (typeof attrs.hand !== "undefined") {
                attrs.hand = [].concat(attrs.hand.match(/../g));
            }
        }
        
        return attrs;
    }
});

module.exports = bookshelf.model("Player", Player);
