var express = require("express"),
    cookieParser = require("cookie-parser"),
    bodyParser = require("body-parser"),
    session = require("express-session"),
    sessionStore = require("./session-store"),
    passport = require("./passport");
    

var app = express();

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(session({
    secret: sessionStore.SECRET,
    resave: true,
    saveUninitialized: true,
    store: sessionStore
}));

app.use(passport.initialize());
app.use(passport.session());

var server = require("http").Server(app); // eslint-disable-line new-cap

module.exports = { app: app, server: server };
