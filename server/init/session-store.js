var config = require("../config.json");

var session = require("express-session"),
    mysql = require("connect-mysql"),
    bcrypt = require("bcrypt-nodejs");

var SECRET = bcrypt.genSaltSync(10);

var SessionStore = mysql(session);

var sessionStoreObj = new SessionStore({
    secret: SECRET,
    pool: true,
    config: config.db
});

sessionStoreObj.SECRET = SECRET;

module.exports = sessionStoreObj;
