var server = require("./express").server,
    passport = require("./passport"),
    sessionStore = require("./session-store"),
    socketio = require("socket.io"),
    passportSocketIo = require("passport.socketio"),
    cookieParser = require("cookie-parser");

var io = socketio(server);

io.set("authorization", passportSocketIo.authorize({
    cookieParser: cookieParser,
    store: sessionStore,
    passport: passport,
    secret: sessionStore.SECRET
}));

module.exports = io;
