var config = require("./config.json");

var fs = require("fs"),
    path = require("path");

var express = require("express"),
    app = require("./init/express").app,
    server = require("./init/express").server,
    io = require("./init/socketio");


var socketControllers = {};
var socketConnectActions = [];
var socketDisconnectActions = [];

function decamelize(str, sep) {
    if (typeof str !== "string") {
        throw new TypeError("Expected a string");
    }

    var separator = typeof sep === "undefined" ? "_" : sep;

    return str
        .replace(/([a-z\d])([A-Z])/g, "$1" + separator + "$2")
        .replace(/([A-Z]+)([A-Z][a-z\d]+)/g, "$1" + separator + "$2")
        .toLowerCase();
}

fs.readdirSync("./controllers/").forEach(function(controllerFile) {
    
    var controllerName = path.basename(controllerFile, ".js");    
    var controller = require("./controllers/" + controllerName);
    
    for (var action in controller) {
        if (controller.hasOwnProperty(action) && {}.toString.call(controller[action]) === "[object Function]") {
            var underscoreAction = decamelize(action, "-");

            var method = underscoreAction.substr(0, underscoreAction.indexOf("-"));
            var actionName = underscoreAction.substr(method.length + 1);

            if (["get", "post", "put", "delete", "socket"].indexOf(method) >= 0) {

                var route = controllerName + "/" + actionName;

                console.log("Adding action " + route + " with method " + method);

                if (method === "socket") {
                    if (actionName === "connect") {
                        socketConnectActions.push(controller[action]);
                    } else if (actionName === "disconnect") {
                        socketDisconnectActions.push(controller[action]);
                    } else {
                        socketControllers[route] = controller[action];
                    }
                } else {
                    app[method]("/" + route, controller[action]);
                }
            }
        }
    }
    
});

io.on("connection", function(socket) {
    socket.user = socket.request.user;
    console.log("NEW CONNECTION: " + JSON.stringify(socket.request.user));
    
    function registerSocketAction(route, action) {
        socket.on(route, function() {
            console.log("route: " + route + " => " + JSON.stringify(arguments));
            action.apply(socket, arguments);
        });
    }
    
    for (var route in socketControllers) {
        if (socketControllers.hasOwnProperty(route)) {
            registerSocketAction(route, socketControllers[route]);
        }
    }
    
    for (var connectActionIndex = 0; connectActionIndex < socketConnectActions.length; ++connectActionIndex) {
        socketConnectActions[connectActionIndex].call(socket, socket);
    }
    
    socket.on("disconnect", function() {
        for (var disconnectActionIndex = 0; disconnectActionIndex < socketDisconnectActions.length; ++disconnectActionIndex) {
            socketDisconnectActions[disconnectActionIndex].call(socket, socket);
        }
    });
});


if (config.production) {
    console.log("Serving production files");
    app.use(express.static(__dirname + "/../dist"));
} else {
    app.use(express.static(__dirname + "/../src"));
}

server.listen(config.port, function() {
    console.log("Listening on port " + config.port + "!");
});
